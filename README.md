<p align="center">
<img src="assets/helping_babies_survive.png"></img>
</p>
<h1 align="center">
 Essential Care for Every Baby
</h1>

[![Download](https://img.shields.io/badge/Download-@latest_APK-green.svg)](https://gitlab.com/kishanhitk/eceb/-/jobs/artifacts/master/raw/build/app/outputs/flutter-apk/app-release.apk?job=build) 


---



The **Essential Care for Every Baby (ECEB)** educational and training program, developed by the American Academy of Pediatrics, provides knowledge, skills, and competencies to nurses and doctors in low/middle-income settings so that they can provide life-saving care to newborns from birth through 24 hours postnatal.

The ECEB Android app is purposely built to provide clinical decision-support for nurses and doctors delivering essential newborn care interventions during the first day of life. 





## Screenshots

| ![Screenshot_1613474145](screenshots/screenshot1.png) | ![Screenshot_1613474154](screenshots/screenshot2.png) | ![Screenshot_1613474168](screenshots/screenshot3.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Welcome Screen                                               | Login Screen                                                 | Home Page                                                    |
| ![Screenshot_1613474173](screenshots/screenshot4.png) | ![Screenshot_1613474185](screenshots/screenshot5.png) | ![Screenshot_1613474393](screenshots/screenshot6.png) |
| List of Babies Screen                                        | Profile Page                                                 | Notifications Screen                                         |




## Setup
Prerequisite:

* [Git](https://git-scm.com/)
* [Flutter SDK](https://flutter.dev/docs/get-started/install)

1. Clone the repo.

   ```bash
   git clone https://gitlab.com/kishanhitk/eceb
   ```

2. In the root project directory, run this command to download packages.

   ```bash
   flutter pub get
   ```

3. Connect an Android Emulator or a physical Android device via ADB and run this command to run the debug version of the app on the connected device.

   ```bash
   flutter run
   ```

4. (Optional) Build release APK. This will create a fat APK which you can install directly on your Android device. 

   ```bash
   flutter build apk --release
   ```



