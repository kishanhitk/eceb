import 'package:eceb/const/colors.dart';
import 'package:eceb/screens/home_screen/home_screen.dart';
import 'package:eceb/screens/list_of_babies_screen/list_of_babies_screen.dart';
import 'package:eceb/screens/notification_screen/notification_screen.dart';
import 'package:eceb/screens/profile_screen/profile_screen.dart';
import 'package:flutter/material.dart';

class BaseWidget extends StatefulWidget {
  static const String routeName = '/base';

  @override
  _BaseWidgetState createState() => _BaseWidgetState();
}

class _BaseWidgetState extends State<BaseWidget> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: IndexedStack(
            index: _selectedIndex,
            children: <Widget>[
              HomeScreen(),
              BabyListScreen(),
              NotificationScreen(),
              ProfileScreen()
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: [
            const BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Home",
            ),
            const BottomNavigationBarItem(
              icon: Icon(Icons.list_alt),
              label: "List of Babies",
            ),
            const BottomNavigationBarItem(
              icon: Icon(Icons.notifications),
              label: "Notifications",
            ),
            const BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Profile",
            ),
          ],
          unselectedItemColor: kDarkGrey,
          currentIndex: _selectedIndex,
          selectedItemColor: kAccentColor,
          onTap: _onItemTapped,
          showUnselectedLabels: true,
          selectedLabelStyle: const TextStyle(
            fontSize: 11,
            fontWeight: FontWeight.w700,
          ),
          unselectedLabelStyle: const TextStyle(
            fontSize: 10,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
