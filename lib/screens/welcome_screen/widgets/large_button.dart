import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';

class LargeButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  const LargeButton({
    Key key,
    this.text,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 15.0,
        vertical: 10,
      ),
      child: Material(
        elevation: 1,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(25),
          ),
        ),
        child: OutlinedButton(
          style: OutlinedButton.styleFrom(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(25),
              ),
            ),
            side: const BorderSide(
              color: kGreyBorderColor,
            ),
            primary: kPrimaryColor,
          ),
          onPressed: () => onPressed(),
          child: SizedBox(
            height: getScreenHeight(context) / 6.5,
            width: getScreenWidth(context) / 1.4,
            child: Center(
              child: Text(
                text,
                style: const TextStyle(
                  color: kPrimaryTextColor,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
