import 'package:eceb/const/colors.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class PolicyText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        children: [
          const TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 11,
              ),
              text: "By continuing, you agree to our "),
          TextSpan(
            style: const TextStyle(
              color: kPrimaryTextColor,
              fontSize: 11,
            ),
            text: "Privacy Policies, ",
            recognizer: TapGestureRecognizer()..onTap = () {},
          ),
          const TextSpan(
              style: TextStyle(
                color: kPrimaryTextColor,
                fontSize: 11,
              ),
              text: "Data Use "),
          const TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 11,
              ),
              text: "including our "),
          const TextSpan(
              style: TextStyle(
                color: kPrimaryTextColor,
                fontSize: 11,
              ),
              text: "Cookies Use"),
        ],
      ),
    );
  }
}
