import 'package:eceb/screens/facility_login_screen/facility_login_screen.dart';
import 'package:eceb/screens/individual_login_screen/individual_login_screen.dart';
import 'package:eceb/screens/welcome_screen/widgets/large_button.dart';
import 'package:eceb/screens/welcome_screen/widgets/polcy_text.dart';
import 'package:eceb/screens/welcome_screen/widgets/welcome_header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            WelcomeHeader(),
            const Spacer(),
            LargeButton(
              text: 'Individual',
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(IndividualLoginScreen.routeName);
              },
            ),
            LargeButton(
              text: 'Facility',
              onPressed: () {
                Navigator.of(context).pushNamed(FacilityLoginScreen.routeName);
              },
            ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 60,
                vertical: 20,
              ),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: PolicyText(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
