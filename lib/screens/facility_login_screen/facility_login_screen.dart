import 'package:eceb/screens/base_widget/base_widget.dart';
import 'package:eceb/screens/welcome_screen/widgets/polcy_text.dart';
import 'package:eceb/utils/size.dart';
import 'package:eceb/widgets/login_header.dart';
import 'package:eceb/widgets/login_text_field.dart';
import 'package:eceb/widgets/signin_button.dart';
import 'package:flutter/material.dart';

class FacilityLoginScreen extends StatelessWidget {
  static const routeName = '/facility_login';
  final TextEditingController employeeCodeController =
      TextEditingController(text: "123456");
  final TextEditingController passwordController =
      TextEditingController(text: "123456");
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: SizedBox(
            height: getScreenHeight(context),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                LoginHeader(),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 30.0,
                    vertical: 10,
                  ),
                  child: LoginTextField(
                    controller: employeeCodeController,
                    isObscure: true,
                    hintText: 'Enter Employee Code',
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 30.0,
                    vertical: 10,
                  ),
                  child: LoginTextField(
                    controller: passwordController,
                    isObscure: true,
                    hintText: "Enter The Password",
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: SignInButton(
                    onPressed: () => {
                      Navigator.pushNamed(
                        context,
                        BaseWidget.routeName,
                      ),
                    },
                  ),
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 60,
                    vertical: 20,
                  ),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: PolicyText(),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
