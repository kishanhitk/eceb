import 'package:eceb/const/colors.dart';
import 'package:eceb/screens/profile_screen/widgets/baby_activity_list.dart';
import 'package:eceb/utils/size.dart';
import 'package:eceb/widgets/custom_dropdown.dart';
import 'package:flutter/material.dart';

import 'active_baby_count_label.dart';

class ProfileActivityCard extends StatefulWidget {
  const ProfileActivityCard({
    Key key,
  }) : super(key: key);

  @override
  _ProfileActivityCardState createState() => _ProfileActivityCardState();
}

class _ProfileActivityCardState extends State<ProfileActivityCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      elevation: 5,
      child: SizedBox(
        width: getScreenWidth(context) * 0.85,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: getScreenHeight(context) * 0.02,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const Text(
                  "Activity",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const CustomDropdown(
                  color: kDropDownColor,
                )
              ],
            ),
            const ActiveBabyCountLabel(),
            const Divider(
              color: Colors.black,
              endIndent: 25,
              indent: 25,
            ),
            const SizedBox(child: BabyActivityList())
          ],
        ),
      ),
    );
  }
}
