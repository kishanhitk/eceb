import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfileInfoCard extends StatefulWidget {
  const ProfileInfoCard({
    Key key,
  }) : super(key: key);

  @override
  _ProfileInfoCardState createState() => _ProfileInfoCardState();
}

class _ProfileInfoCardState extends State<ProfileInfoCard> {
  bool _isSharingLocation = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        elevation: 5,
        child: SizedBox(
          width: getScreenWidth(context) * 0.85,
          height: getScreenHeight(context) * 0.25,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15, top: 8, bottom: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Account Details",
                      style: GoogleFonts.poppins(fontWeight: FontWeight.bold),
                    ),
                    Text("Name: Fiona",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold, color: kDarkGrey)),
                    Text("ID: *****124",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold, color: kDarkGrey)),
                    Row(
                      children: [
                        Text("Share Location",
                            style: GoogleFonts.poppins(
                                fontWeight: FontWeight.bold, color: kDarkGrey)),
                        Switch(
                            activeColor: kAccentColor,
                            value: _isSharingLocation,
                            onChanged: (val) {
                              setState(() {
                                _isSharingLocation = val;
                              });
                            })
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: CircleAvatar(
                  radius: getScreenHeight(context) * 0.0625,
                  backgroundImage:
                      const AssetImage('assets/profile_photo2.jpg'),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Column(
                  children: [
                    IconButton(
                        visualDensity: VisualDensity.compact,
                        icon: const Icon(
                          Icons.settings,
                          color: kLightBlueAccent,
                        ),
                        onPressed: () {}),
                    IconButton(
                        icon: const Icon(
                          Icons.edit,
                          color: kLightBlueAccent,
                        ),
                        onPressed: () {}),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
