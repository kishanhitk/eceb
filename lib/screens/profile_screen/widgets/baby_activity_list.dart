import 'package:eceb/const/colors.dart';
import 'package:flutter/material.dart';

class BabyActivityList extends StatelessWidget {
  const BabyActivityList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (ctx, i) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 5),
            child: Column(
              children: [
                const Text(
                  "Baby 1 of Oni registered at the Post natal ward  at 11:53 AM",
                  textAlign: TextAlign.justify,
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: kDarkGrey),
                ),
                const Align(
                    alignment: Alignment.bottomRight,
                    child: Text(
                      "19/05/2019  11:53 AM",
                      style: TextStyle(
                        fontWeight: FontWeight.w100,
                      ),
                    ))
              ],
            ),
          );
        },
        separatorBuilder: (ctx, i) {
          return const Divider(
            color: Colors.black,
            endIndent: 25,
            indent: 25,
          );
        },
        itemCount: 100);
  }
}
