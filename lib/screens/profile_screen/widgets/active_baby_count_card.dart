import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ActivityBabyCountCard extends StatelessWidget {
  final String label;
  final int count;
  const ActivityBabyCountCard({
    Key key,
    this.label,
    this.count,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          elevation: 5,
          child: SizedBox(
            height: getScreenHeight(context) * 0.14,
            width: getScreenWidth(context) * 0.25,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 8.0,
                    right: 8.0,
                    top: 8.0,
                  ),
                  child: Text(
                    label ?? "Registered babies:",
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.bold,
                      color: kDarkGrey,
                      fontSize: 13,
                    ),
                    maxLines: 2,
                  ),
                ),
                Text(
                  count.toString() ?? '5',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                )
              ],
            ),
          )),
    );
  }
}
