import 'package:eceb/screens/profile_screen/widgets/active_baby_count_card.dart';
import 'package:flutter/material.dart';

class ActiveBabyCountLabel extends StatelessWidget {
  const ActiveBabyCountLabel({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        const ActivityBabyCountCard(
          label: "Registered babies:",
          count: 5,
        ),
        const ActivityBabyCountCard(
          label: "Diagonised/Change List:",
          count: 6,
        ),
        const ActivityBabyCountCard(
          label: "Discharged babies:",
          count: 3,
        )
      ],
    );
  }
}
