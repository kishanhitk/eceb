import 'package:eceb/screens/profile_screen/widgets/profile_activity_card.dart';
import 'package:eceb/screens/profile_screen/widgets/profile_info_card.dart';
import 'package:eceb/widgets/home_app_bar.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              'assets/home_backdrop.png',
            ),
          ),
          Column(
            children: [
              const HomeAppBar(
                icon: Icon(
                  Icons.menu,
                  color: Colors.white,
                ),
                label: "ECEB",
              ),
              Expanded(
                child: ListView(
                  children: [
                    Column(
                      children: [
                        const ProfileInfoCard(),
                        const ProfileActivityCard()
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
