import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';

class RegisterBabyLabel extends StatelessWidget {
  const RegisterBabyLabel({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getScreenHeight(context) / 4,
      width: getScreenWidth(context),
      decoration: const BoxDecoration(color: kPrimaryColor),
      child: Padding(
        padding: const EdgeInsets.all(35.0),
        child: ElevatedButton(
          onPressed: () {},
          style: ElevatedButton.styleFrom(
            primary: Colors.white,
            onPrimary: kPrimaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/form.png',
                height: getScreenWidth(context) / 6,
              ),
              const SizedBox(
                width: 9,
              ),
              const Text(
                'To Register a Baby',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
