import 'package:eceb/const/colors.dart';
import 'package:flutter/material.dart';

class OnCallDoctorLabel extends StatelessWidget {
  const OnCallDoctorLabel({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          "On-Call Doctors",
          style: TextStyle(
              color: kDarkGrey, fontSize: 15, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 15,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const DoctorAvatar(
              imgUrl: 'assets/doctor1.png',
              doctorName: "Andrea",
              isOnline: true,
            ),
            const DoctorAvatar(
              imgUrl: 'assets/doctor2.png',
              doctorName: "Kim",
              isOnline: true,
            ),
            const DoctorAvatar(
              imgUrl: 'assets/doctor3.png',
              doctorName: "Jane",
              isOnline: true,
            ),
          ],
        )
      ],
    );
  }
}

class DoctorAvatar extends StatelessWidget {
  final String imgUrl;
  final String doctorName;
  final bool isOnline;

  const DoctorAvatar({
    Key key,
    this.imgUrl,
    this.doctorName,
    this.isOnline,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              padding: const EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
              )),
          onPressed: () {},
          child: CircleAvatar(
            radius: 45,
            backgroundImage: AssetImage(imgUrl),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Text(
          doctorName,
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        Text(
          isOnline ? "Online" : "Offline",
          style: const TextStyle(color: kAccentColor, height: 0.9),
        )
      ],
    );
  }
}
