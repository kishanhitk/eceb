import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';

class SummaryWidget extends StatelessWidget {
  final int admittedCount;
  final int dischargedCount;
  final int highriskCount;
  const SummaryWidget({
    Key key,
    this.admittedCount,
    this.dischargedCount,
    this.highriskCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          "Summary of 24 hours",
          style: TextStyle(
              color: kDarkGrey, fontSize: 15, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SummaryCard(
              label: "Admitted:",
              number: admittedCount.toString(),
              color: kAdmittedColor,
            ),
            SummaryCard(
              label: "Discharged:",
              number: dischargedCount.toString(),
              color: kDischargedColor,
            ),
            SummaryCard(
              label: "High Risk:",
              number: highriskCount.toString(),
              color: kHighRiskColor,
            )
          ],
        )
      ],
    );
  }
}

class SummaryCard extends StatelessWidget {
  final String label;
  final String number;
  final Color color;
  const SummaryCard({
    Key key,
    this.label,
    this.number,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {},
      style: ElevatedButton.styleFrom(
        elevation: 7,
        padding: const EdgeInsets.all(0),
        primary: Colors.white,
        onPrimary: kPrimaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
      ),
      child: SizedBox(
        height: 110 ?? getScreenHeight(context) / 6.7,
        width: 100 ?? getScreenWidth(context) / 3.7,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ColorFiltered(
              colorFilter: ColorFilter.mode(color, BlendMode.srcIn),
              child: Image.asset('assets/baby_with_diaper.png'),
            ),
            const SizedBox(
              height: 3,
            ),
            Text(
              label,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            Text(
              number,
              style: const TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
