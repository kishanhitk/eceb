import 'dart:ui';
import 'package:eceb/screens/home_screen/widget/on_call_doctor_label.dart';
import 'package:eceb/screens/home_screen/widget/register_baby_label.dart';
import 'package:eceb/screens/home_screen/widget/summary_widget.dart';
import 'package:eceb/widgets/home_app_bar.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              'assets/home_backdrop.png',
            ),
          ),
          Column(
            children: [
              const HomeAppBar(
                icon: Icon(
                  Icons.menu,
                  color: Colors.white,
                ),
                label: "ECEB",
              ),
              const SizedBox(
                height: 10,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 30.0,
                ),
                child: SummaryWidget(
                  admittedCount: 14,
                  dischargedCount: 20,
                  highriskCount: 5,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const RegisterBabyLabel(),
              const SizedBox(
                height: 20,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 30.0),
                child: OnCallDoctorLabel(),
              )
            ],
          ),
        ],
      ),
    );
  }
}
