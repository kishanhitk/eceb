import 'package:eceb/screens/list_of_babies_screen/widgets/past_registered.dart';
import 'package:eceb/screens/list_of_babies_screen/widgets/search_and_sort.dart';
import 'package:eceb/const/colors.dart';
import 'package:eceb/screens/list_of_babies_screen/widgets/recently_added.dart';
import 'package:eceb/widgets/home_app_bar.dart';
import 'package:flutter/material.dart';

class BabyListScreen extends StatefulWidget {
  @override
  _BabyListScreenState createState() => _BabyListScreenState();
}

class _BabyListScreenState extends State<BabyListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const HomeAppBar(
            label: "List of Babies",
            icon: Icon(
              Icons.menu,
              color: kPrimaryColor,
            ),
          ),
          Expanded(
              child: ListView(
            children: [
              Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: SearchAndSortListOfBabies(),
                  ),
                  // RecentlyAdded(),
                  const RecentlyAddedView(),
                  const PastRegisteredView()
                ],
              )
            ],
          )),
        ],
      ),
    );
  }
}
