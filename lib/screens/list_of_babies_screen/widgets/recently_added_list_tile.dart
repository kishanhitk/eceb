import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class RecentlyAddedListTile extends StatelessWidget {
  final String motherName;
  final bool isMale;
  const RecentlyAddedListTile({
    Key key,
    @required this.motherName,
    @required this.isMale,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
          15,
        )),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: kPrimaryColor.withOpacity(0.26),
                  borderRadius: BorderRadius.circular(15)),
              width: getScreenWidth(context),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5),
                child: Text(
                  "22 mins from birth",
                  style: GoogleFonts.poppins(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 15.0,
                    horizontal: 2,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(children: [
                          TextSpan(
                            text: "Baby  ",
                            style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontSize: 17,
                            ),
                          ),
                          TextSpan(
                            text: "of",
                            style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontSize: 10,
                            ),
                          ),
                          TextSpan(
                            text: "  $motherName",
                            style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontSize: 17,
                            ),
                          ),
                        ]),
                      ),
                      RichText(
                        text: TextSpan(children: [
                          TextSpan(
                            text: "Location:  ",
                            style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontSize: 13,
                            ),
                          ),
                          TextSpan(
                            text: "Prenatal Ward",
                            style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ]),
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    FaIcon(
                      isMale ? FontAwesomeIcons.male : FontAwesomeIcons.female,
                    ),
                    Text(
                      isMale ? "Male" : "Female",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
                IconButton(
                    visualDensity: VisualDensity.compact,
                    icon: const Icon(Icons.arrow_forward_ios),
                    onPressed: () {})
              ],
            )
          ],
        ),
      ),
    );
  }
}
