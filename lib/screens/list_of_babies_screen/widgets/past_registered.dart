import 'package:eceb/const/colors.dart';
import 'package:eceb/screens/list_of_babies_screen/widgets/past_registered_list_tile.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PastRegisteredView extends StatelessWidget {
  const PastRegisteredView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            left: getScreenWidth(context) * 0.1,
            top: 10,
          ),
          child: Text(
            "Past Registered",
            style: GoogleFonts.poppins(
              fontSize: 17,
              fontWeight: FontWeight.w700,
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          child: ListView(
            padding: EdgeInsets.only(
              top: getScreenHeight(context) * 0.01,
            ),
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: [
              const PastRegisteredListTile(
                isMale: true,
                motherName: "Oni",
                color: Colors.red ?? kRedDanger,
              ),
              const PastRegisteredListTile(
                isMale: false,
                color: kGreenNormal,
                motherName: "Nia",
              ),
              const PastRegisteredListTile(
                isMale: false,
                color: kYellowProblem,
                motherName: "Nia",
              ),
              const PastRegisteredListTile(
                isMale: false,
                color: Colors.red ?? kRedDanger,
                motherName: "Nia",
              ),
              const PastRegisteredListTile(
                isMale: false,
                color: kYellowProblem,
                motherName: "Nia",
              ),
            ],
          ),
        ),
      ],
    );
  }
}
