import 'package:eceb/screens/list_of_babies_screen/widgets/recently_added_list_tile.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RecentlyAddedView extends StatelessWidget {
  const RecentlyAddedView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            left: getScreenWidth(context) * 0.1,
            top: 10,
          ),
          child: Text(
            "Recently Added",
            style: GoogleFonts.poppins(
              fontSize: 17,
              fontWeight: FontWeight.w700,
              color: Colors.black,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getScreenWidth(context) * 0.05,
          ),
          child: SizedBox(
            child: ListView(
              padding: EdgeInsets.only(
                top: getScreenHeight(context) * 0.01,
              ),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                const RecentlyAddedListTile(
                  isMale: true,
                  motherName: "Oni",
                ),
                const RecentlyAddedListTile(
                  isMale: false,
                  motherName: "Nia",
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
