import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:eceb/widgets/custom_dropdown.dart';
import 'package:flutter/material.dart';

class SearchAndSortListOfBabies extends StatelessWidget {
  const SearchAndSortListOfBabies({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          height: 30,
          width: getScreenWidth(context) * 0.6,
          decoration: BoxDecoration(
            color: kPrimaryColor,
            borderRadius: BorderRadius.circular(25),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const Text(
                "Search the list of babies",
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
              const Icon(
                Icons.search,
                color: Colors.white,
              )
            ],
          ),
        ),
        const CustomDropdown(
          color: kPrimaryColor,
        )
      ],
    );
  }
}
