import 'package:eceb/const/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

enum Status { Normal, Problem, Danger }

class RiskAssesmentListTile extends StatefulWidget {
  final String motherName;
  final String time;
  final String location;
  final Status prevStatus;
  final Status currentStatus;
  final Color color;

  const RiskAssesmentListTile({
    Key key,
    this.motherName,
    this.time,
    this.location,
    this.prevStatus,
    this.currentStatus,
    this.color,
  }) : super(key: key);

  @override
  _RiskAssesmentListTileState createState() => _RiskAssesmentListTileState();
}

class _RiskAssesmentListTileState extends State<RiskAssesmentListTile> {
  bool _isSelected = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundColor: widget.color,
            ),
            const SizedBox(
              width: 12,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Baby of ${widget.motherName}",
                  style: GoogleFonts.poppins(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Location: ${widget.location}",
                  style: GoogleFonts.poppins(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 11,
                  ),
                ),
                Text(
                  widget.time,
                  style: GoogleFonts.poppins(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 11,
                  ),
                )
              ],
            ),
            const SizedBox(
              width: 12,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 11,
                          ),
                          text: "Status Changed:\n"),
                      TextSpan(
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 11,
                          ),
                          text:
                              "${widget.prevStatus.toString().substring(7)} to "),
                      TextSpan(
                          style: GoogleFonts.poppins(
                            color: widget.currentStatus == Status.Danger
                                ? Colors.red
                                : Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 11,
                          ),
                          text: widget.currentStatus.toString().substring(7)),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              width: 7,
            ),
            Checkbox(
              activeColor: kAccentColor,
              visualDensity: VisualDensity.compact,
              value: _isSelected,
              onChanged: (val) {
                setState(() {
                  _isSelected = val;
                });
              },
            )
          ],
        ),
        const Divider(
          height: 35,
          color: Colors.black,
        ),
      ],
    );
  }
}
