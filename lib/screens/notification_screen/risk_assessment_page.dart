import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';
import 'widgets/risk_assessment_list_tile.dart';

class RiskAssesmentPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(left: 45.0, top: 15),
          child: Text(
            "Today",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        const Divider(
          height: 35,
          color: Colors.black,
        ),
        Container(
          color: Colors.red,
          height: 100,
        ),
        Expanded(
          child: ListView(
            padding: EdgeInsets.only(
              top: getScreenHeight(context) * 0.01,
            ),
            shrinkWrap: true,
            children: [
              const RiskAssesmentListTile(
                motherName: "Oni",
                location: "Prenatal Ward",
                time: "EAT 11:06 AM",
                prevStatus: Status.Problem,
                currentStatus: Status.Normal,
                color: Colors.green,
              ),
              const RiskAssesmentListTile(
                motherName: "Ada",
                location: "Prenatal Ward",
                time: "EAT 10:56 AM",
                prevStatus: Status.Problem,
                currentStatus: Status.Danger,
                color: Colors.red,
              ),
              const RiskAssesmentListTile(
                motherName: "Teka",
                location: "Prenatal Ward",
                time: "EAT 10:56 AM",
                prevStatus: Status.Normal,
                currentStatus: Status.Problem,
                color: Colors.yellow,
              ),
              const RiskAssesmentListTile(
                motherName: "Ada",
                location: "Prenatal Ward",
                time: "EAT 10:56 AM",
                prevStatus: Status.Problem,
                currentStatus: Status.Normal,
                color: Colors.green,
              ),
              const RiskAssesmentListTile(
                motherName: "Ada",
                location: "Prenatal Ward",
                time: "EAT 10:56 AM",
                prevStatus: Status.Problem,
                currentStatus: Status.Danger,
                color: Colors.red,
              ),
              const RiskAssesmentListTile(
                motherName: "Ada",
                location: "Prenatal Ward",
                time: "EAT 10:56 AM",
                prevStatus: Status.Normal,
                currentStatus: Status.Problem,
                color: Colors.yellow,
              ),
              const RiskAssesmentListTile(
                motherName: "Ada",
                location: "Prenatal Ward",
                time: "EAT 10:56 AM",
                prevStatus: Status.Problem,
                currentStatus: Status.Danger,
                color: Colors.red,
              ),
              const RiskAssesmentListTile(
                motherName: "Ada",
                location: "Prenatal Ward",
                time: "EAT 10:56 AM",
                prevStatus: Status.Problem,
                currentStatus: Status.Danger,
                color: Colors.red,
              ),
              const RiskAssesmentListTile(
                motherName: "Ada",
                location: "Prenatal Ward",
                time: "EAT 10:56 AM",
                prevStatus: Status.Problem,
                currentStatus: Status.Danger,
                color: Colors.red,
              ),
            ],
          ),
        )
      ],
    );
  }
}
