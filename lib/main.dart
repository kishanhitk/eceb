import 'package:eceb/const/colors.dart';
import 'package:eceb/screens/facility_login_screen/facility_login_screen.dart';
import 'package:eceb/screens/individual_login_screen/individual_login_screen.dart';
import 'package:eceb/screens/welcome_screen/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import 'screens/base_widget/base_widget.dart';

void main() {
  runApp(MyApp());
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: kPrimaryColor,
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Essential Care',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: Colors.blue,
        textTheme: GoogleFonts.poppinsTextTheme(),
      ),
      routes: {
        WelcomeScreen.routeName: (context) => WelcomeScreen(),
        IndividualLoginScreen.routeName: (context) => IndividualLoginScreen(),
        FacilityLoginScreen.routeName: (context) => FacilityLoginScreen(),
        BaseWidget.routeName: (context) => BaseWidget()
      },
      initialRoute: WelcomeScreen.routeName,
    );
  }
}
