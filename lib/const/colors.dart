import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xff82A0C8);
const kPrimaryTextColor = Color(0xff3082CC);
const kGreyBorderColor = Color(0xffECECEC);
const kAccentColor = Color(0xff064294);
const kDarkGrey = Color(0xff707070);
const kHighRiskColor = Color(0xffFF6B6B);
const kDischargedColor = Color(0xff9F9F9F);
const kAdmittedColor = Color(0xff82A0C8);
const kLightBlueAccent = Color(0xffCDD0FC);
const kDropDownColor = Color(0xff8B93F8);
const kRedDanger = Color(0xffEA6165);
const kGreenNormal = Color(0xff3D9A4F);
const kYellowProblem = Color(0xffFED604);
