import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';

class SignInButton extends StatelessWidget {
  final Function onPressed;
  const SignInButton({
    Key key,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 1,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(25),
        ),
      ),
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(25),
            ),
          ),
          side: const BorderSide(
            color: kGreyBorderColor,
          ),
          primary: kPrimaryColor,
        ),
        onPressed: () => onPressed(),
        child: SizedBox(
          height: getScreenHeight(context) / 15,
          width: getScreenWidth(context) / 2.4,
          child: const Center(
            child: Text(
              'Sign In',
              style: TextStyle(
                color: kPrimaryTextColor,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
