import 'package:eceb/const/colors.dart';
import 'package:flutter/material.dart';

class HomeAppBar extends StatelessWidget {
  final Icon icon;
  final String label;
  const HomeAppBar({
    Key key,
    @required this.label,
    @required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0, right: 10, left: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              width: 5,
            ),
            IconButton(
              icon: icon ??
                  const Icon(
                    Icons.menu,
                    size: 30,
                    color: Colors.white,
                  ),
              onPressed: () {},
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  label,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const Text(
                  "ID: ****1234",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                )
              ],
            ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.only(
                bottom: 10.0,
                right: 10,
              ),
              child: SizedBox(
                  height: 70,
                  child: Image.asset('assets/helping_babies_survive.png')),
            )
          ],
        ),
      ),
    );
  }
}
