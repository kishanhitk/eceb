import 'package:eceb/const/colors.dart';
import 'package:eceb/utils/size.dart';
import 'package:flutter/material.dart';

class LoginHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: getScreenWidth(context),
      height: getScreenHeight(context) / 1.8,
      decoration: const BoxDecoration(
        color: kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(35),
          bottomRight: Radius.circular(35),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(
            height: getScreenHeight(context) / 4,
            child: Image.asset('assets/helping_babies_survive.png'),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 20),
            child: Text(
              "ECEB",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w900,
                fontSize: 85,
              ),
            ),
          )
        ],
      ),
    );
  }
}
