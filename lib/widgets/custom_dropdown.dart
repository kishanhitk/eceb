import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomDropdown extends StatefulWidget {
  final Color color;

  const CustomDropdown({Key key, @required this.color}) : super(key: key);
  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  String _dropDownValue = 'Status';

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: widget.color, borderRadius: BorderRadius.circular(25)),
      child: Padding(
        padding: const EdgeInsets.only(
          top: 4,
          bottom: 4,
          left: 8,
          right: 4,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Sort by: ',
              style: TextStyle(color: Colors.white),
            ),
            DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                iconSize: 0,
                isDense: true,
                focusColor: widget.color,
                dropdownColor: widget.color,
                value: _dropDownValue,
                icon: const Icon(Icons.arrow_downward),
                elevation: 16,
                style: GoogleFonts.poppins(color: Colors.white),
                onChanged: (String newValue) {
                  setState(() {
                    _dropDownValue = newValue;
                  });
                },
                items: <String>['Date', 'Status']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
